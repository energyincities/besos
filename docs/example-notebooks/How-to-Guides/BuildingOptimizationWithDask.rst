Building Optimization with Dask
===============================

This notebook uses Dask to parrallize the NSGAII alogrithm. Most of this
notebook is copied from the "BuildingOptimization" notebook. But, this
notebook will skip over concepts unrelated to Dask. Go to
examples/Optimization/BuildingOptimization.ipynb for more details about
the notebook.

Setup dask
~~~~~~~~~~

To use Dask setup the scheduler and the workers by runnning the cell
below. The client object is used to get and set various dask settings
such as the number of workers.

If you're running this notebook locally, you should be able to open the
dashboard using the link provided by client.

.. code:: ipython3

    import pandas as pd
    from besos import eppy_funcs as ef
    from besos.evaluator import EvaluatorEP
    from besos.optimizer import NSGAII
    from besos.parameters import RangeParameter, expand_plist, wwr
    from besos.problem import EPProblem
    from matplotlib import pyplot as plt

    from dask.distributed import Client
    client = Client()
    client




.. raw:: html

    <table style="border: 2px solid white;">
    <tr>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Client</h3>
    <ul style="text-align: left; list-style: none; margin: 0; padding: 0;">
      <li><b>Scheduler: </b>tcp://127.0.0.1:42772</li>
      <li><b>Dashboard: </b><a href='/user/peterrwilson99/proxy/8787/status' target='_blank'>/user/peterrwilson99/proxy/8787/status</a></li>
    </ul>
    </td>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Cluster</h3>
    <ul style="text-align: left; list-style:none; margin: 0; padding: 0;">
      <li><b>Workers: </b>4</li>
      <li><b>Cores: </b>16</li>
      <li><b>Memory: </b>68.72 GB</li>
    </ul>
    </td>
    </tr>
    </table>



Building Optimization setup
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    building = ef.get_building("in.idf")  # Load the E+ model in.idf

.. code:: ipython3

    parameters = []
    parameters = expand_plist(  # Use helper function to make parameter list
        {
            "Building 1": {"North Axis": (0, 359)}  # Name from IDF Building object
        }  # Change orientation from North
    )

    parameters.append(
        wwr(RangeParameter(0.1, 0.9))
    )  # Add window-to-wall ratio as a parameter between 0.1 and 0.9 using a custom function

.. code:: ipython3

    objectives = [
        "DistrictCooling:Facility",
        "DistrictHeating:Facility",
    ]  # Use Heating and Cooling (Ideal air loads) as objectives
    problem = EPProblem(
        parameters, objectives
    )  # Make a problem instance from the parameters and objectives

Set up EnergyPlus evaluator
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Energy Plus evaluator must have more then one process to enable
multiprocessing. This disables the caching functionality of the
evaluator which is incompatible with multiprocessing.

In this cell, a evaluator is created and one simulation is run as a test

.. code:: ipython3

    evaluatorEP = EvaluatorEP(
        problem, building
    )  # outputdir must exist; E+ files will be written there
    runs = pd.DataFrame.from_dict(
        {"0": [180, 0.5]}, orient="index"
    )  # Make a dataframe of runs with one entry for South and 50% glazing
    outputs = evaluatorEP.df_apply(runs)  # Run this as a test
    outputs



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=1.0, style=ProgressStyle(description_widt…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>DistrictCooling:Facility</th>
          <th>DistrictHeating:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>3.233564e+09</td>
          <td>4.931726e+09</td>
        </tr>
      </tbody>
    </table>
    </div>



Run the Genetic Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~

Run the genetic algorithm. Because the evaluator was created with more
then one process the algorithm will also use multiprocessing For each
iteration of the algorithm, the evaluator will be run in parrallel for
each worker available (indicated by the Dask client objct).

You might also see a bunch of warnings along the lines of "..full
garbage collections...". I'm not sure exactly what's causing them, but
restarting the workers from time to time will help minimize the warnings
~ goh.

.. code:: ipython3

    %%time
    results = NSGAII(
        evaluatorEP, evaluations=20, population_size=50
    )  # Run the optimizer using this evaluator for a population size of 20 for 10 generations
    results


.. parsed-literal::

    distributed.utils_perf - WARNING - full garbage collections took 46% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 47% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)


.. parsed-literal::

    CPU times: user 1min 17s, sys: 7.21 s, total: 1min 24s
    Wall time: 4min 22s




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>North Axis</th>
          <th>RangeParameter [0.1, 0.9]</th>
          <th>DistrictCooling:Facility</th>
          <th>DistrictHeating:Facility</th>
          <th>violation</th>
          <th>pareto-optimal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>71.382076</td>
          <td>0.437110</td>
          <td>4.358714e+09</td>
          <td>3.816222e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>1</th>
          <td>299.840962</td>
          <td>0.701673</td>
          <td>4.588401e+09</td>
          <td>4.781656e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>2</th>
          <td>219.852640</td>
          <td>0.601671</td>
          <td>3.635201e+09</td>
          <td>5.377198e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>3</th>
          <td>140.670862</td>
          <td>0.863716</td>
          <td>3.586940e+09</td>
          <td>6.859501e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>4</th>
          <td>150.525194</td>
          <td>0.878006</td>
          <td>3.427311e+09</td>
          <td>6.928445e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>5</th>
          <td>198.223895</td>
          <td>0.476851</td>
          <td>3.349558e+09</td>
          <td>4.801244e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>6</th>
          <td>263.106277</td>
          <td>0.526319</td>
          <td>4.295008e+09</td>
          <td>4.485197e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>7</th>
          <td>273.776699</td>
          <td>0.723453</td>
          <td>4.463784e+09</td>
          <td>5.305216e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>8</th>
          <td>313.888573</td>
          <td>0.633187</td>
          <td>4.542461e+09</td>
          <td>4.261052e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>9</th>
          <td>65.739115</td>
          <td>0.844361</td>
          <td>4.534472e+09</td>
          <td>5.657544e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>10</th>
          <td>244.108264</td>
          <td>0.139270</td>
          <td>4.079191e+09</td>
          <td>2.744689e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>11</th>
          <td>316.225471</td>
          <td>0.356809</td>
          <td>4.472947e+09</td>
          <td>3.005892e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>12</th>
          <td>165.704853</td>
          <td>0.142534</td>
          <td>3.455403e+09</td>
          <td>2.952677e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>13</th>
          <td>176.548539</td>
          <td>0.519485</td>
          <td>3.229829e+09</td>
          <td>5.041344e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>14</th>
          <td>215.470655</td>
          <td>0.240727</td>
          <td>3.649903e+09</td>
          <td>3.459326e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>15</th>
          <td>121.225588</td>
          <td>0.438825</td>
          <td>3.868790e+09</td>
          <td>4.427992e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>16</th>
          <td>1.064533</td>
          <td>0.271421</td>
          <td>4.264107e+09</td>
          <td>2.378304e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>17</th>
          <td>52.152901</td>
          <td>0.657508</td>
          <td>4.459781e+09</td>
          <td>4.557195e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>18</th>
          <td>249.782177</td>
          <td>0.105415</td>
          <td>4.162442e+09</td>
          <td>2.510717e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>19</th>
          <td>143.033784</td>
          <td>0.609786</td>
          <td>3.546025e+09</td>
          <td>5.509415e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>20</th>
          <td>316.680927</td>
          <td>0.578098</td>
          <td>4.512892e+09</td>
          <td>3.967078e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>21</th>
          <td>353.646224</td>
          <td>0.350275</td>
          <td>4.270387e+09</td>
          <td>2.696923e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>22</th>
          <td>138.595062</td>
          <td>0.513111</td>
          <td>3.618218e+09</td>
          <td>4.955166e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>23</th>
          <td>310.415717</td>
          <td>0.124600</td>
          <td>4.493048e+09</td>
          <td>2.036819e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>24</th>
          <td>333.266753</td>
          <td>0.647367</td>
          <td>4.424149e+09</td>
          <td>4.055692e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>25</th>
          <td>102.951632</td>
          <td>0.144138</td>
          <td>4.122826e+09</td>
          <td>2.686456e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>26</th>
          <td>99.412952</td>
          <td>0.195314</td>
          <td>4.143610e+09</td>
          <td>2.920224e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>27</th>
          <td>78.379686</td>
          <td>0.312676</td>
          <td>4.303378e+09</td>
          <td>3.297800e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>28</th>
          <td>302.796971</td>
          <td>0.106689</td>
          <td>4.496140e+09</td>
          <td>2.017476e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>29</th>
          <td>123.990886</td>
          <td>0.234128</td>
          <td>3.860546e+09</td>
          <td>3.337713e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>30</th>
          <td>182.583857</td>
          <td>0.419069</td>
          <td>3.263349e+09</td>
          <td>4.505705e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>31</th>
          <td>349.374379</td>
          <td>0.165442</td>
          <td>4.327295e+09</td>
          <td>1.963931e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>32</th>
          <td>300.042979</td>
          <td>0.520984</td>
          <td>4.523363e+09</td>
          <td>3.929705e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>33</th>
          <td>162.525322</td>
          <td>0.310286</td>
          <td>3.381877e+09</td>
          <td>3.891765e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>34</th>
          <td>293.523382</td>
          <td>0.261707</td>
          <td>4.468832e+09</td>
          <td>2.817049e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>35</th>
          <td>10.117742</td>
          <td>0.300807</td>
          <td>4.260468e+09</td>
          <td>2.529534e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>36</th>
          <td>185.075163</td>
          <td>0.356574</td>
          <td>3.296258e+09</td>
          <td>4.145815e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>37</th>
          <td>86.822357</td>
          <td>0.513505</td>
          <td>4.278318e+09</td>
          <td>4.398771e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>38</th>
          <td>342.138876</td>
          <td>0.202175</td>
          <td>4.351387e+09</td>
          <td>2.138354e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>39</th>
          <td>158.379348</td>
          <td>0.613572</td>
          <td>3.336195e+09</td>
          <td>5.571146e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>40</th>
          <td>48.678043</td>
          <td>0.623399</td>
          <td>4.439759e+09</td>
          <td>4.330768e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>41</th>
          <td>131.721176</td>
          <td>0.403263</td>
          <td>3.726968e+09</td>
          <td>4.324951e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>42</th>
          <td>1.275195</td>
          <td>0.263956</td>
          <td>4.264487e+09</td>
          <td>2.366280e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>43</th>
          <td>216.565201</td>
          <td>0.652247</td>
          <td>3.577040e+09</td>
          <td>5.712885e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>44</th>
          <td>29.507229</td>
          <td>0.861832</td>
          <td>4.449814e+09</td>
          <td>5.129074e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>45</th>
          <td>177.739171</td>
          <td>0.872205</td>
          <td>3.168680e+09</td>
          <td>6.964722e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>46</th>
          <td>103.169745</td>
          <td>0.727197</td>
          <td>4.162633e+09</td>
          <td>5.721400e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>47</th>
          <td>183.466568</td>
          <td>0.639685</td>
          <td>3.203489e+09</td>
          <td>5.699981e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>48</th>
          <td>11.305850</td>
          <td>0.280824</td>
          <td>4.265549e+09</td>
          <td>2.457105e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>49</th>
          <td>199.381870</td>
          <td>0.764400</td>
          <td>3.312673e+09</td>
          <td>6.374030e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
      </tbody>
    </table>
    </div>



Compare with sequential processing. The performance difference will be
greater if your machine has more cores.

.. code:: ipython3

    %%time
    evaluatorEP = EvaluatorEP(problem, building)

    results2 = NSGAII(
        evaluatorEP, evaluations=20, population_size=50
    )  # Run the optimizer using this evaluator for a population size of 20 for 10 generations
    results2


.. parsed-literal::

    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 48% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 49% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 52% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 50% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 51% CPU time recently (threshold: 10%)
    distributed.utils_perf - WARNING - full garbage collections took 52% CPU time recently (threshold: 10%)


.. parsed-literal::

    CPU times: user 1min 18s, sys: 7.16 s, total: 1min 25s
    Wall time: 4min 24s




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>North Axis</th>
          <th>RangeParameter [0.1, 0.9]</th>
          <th>DistrictCooling:Facility</th>
          <th>DistrictHeating:Facility</th>
          <th>violation</th>
          <th>pareto-optimal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>30.985142</td>
          <td>0.163250</td>
          <td>4.349064e+09</td>
          <td>2.082418e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>1</th>
          <td>343.986723</td>
          <td>0.169553</td>
          <td>4.351741e+09</td>
          <td>1.989796e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>2</th>
          <td>132.588809</td>
          <td>0.857802</td>
          <td>3.723984e+09</td>
          <td>6.712684e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>3</th>
          <td>190.390483</td>
          <td>0.866152</td>
          <td>3.210981e+09</td>
          <td>6.919259e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>4</th>
          <td>100.434378</td>
          <td>0.217507</td>
          <td>4.131097e+09</td>
          <td>3.051266e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>5</th>
          <td>140.594568</td>
          <td>0.527184</td>
          <td>3.588418e+09</td>
          <td>5.003672e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>6</th>
          <td>77.647901</td>
          <td>0.516704</td>
          <td>4.345977e+09</td>
          <td>4.285707e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>7</th>
          <td>314.122199</td>
          <td>0.811703</td>
          <td>4.609703e+09</td>
          <td>5.047447e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>8</th>
          <td>169.806001</td>
          <td>0.203200</td>
          <td>3.390162e+09</td>
          <td>3.303781e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>9</th>
          <td>165.192059</td>
          <td>0.464310</td>
          <td>3.304045e+09</td>
          <td>4.733369e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>10</th>
          <td>145.527483</td>
          <td>0.473950</td>
          <td>3.525268e+09</td>
          <td>4.786143e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>11</th>
          <td>200.301543</td>
          <td>0.335061</td>
          <td>3.421391e+09</td>
          <td>3.996262e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>12</th>
          <td>343.129802</td>
          <td>0.348334</td>
          <td>4.323107e+09</td>
          <td>2.727842e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>13</th>
          <td>240.045938</td>
          <td>0.397933</td>
          <td>3.971847e+09</td>
          <td>4.120891e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>14</th>
          <td>258.371548</td>
          <td>0.227977</td>
          <td>4.226768e+09</td>
          <td>3.067001e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>15</th>
          <td>130.386726</td>
          <td>0.247904</td>
          <td>3.776790e+09</td>
          <td>3.453907e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>16</th>
          <td>297.282683</td>
          <td>0.186413</td>
          <td>4.477456e+09</td>
          <td>2.427426e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>17</th>
          <td>349.005589</td>
          <td>0.511571</td>
          <td>4.290375e+09</td>
          <td>3.406691e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>18</th>
          <td>35.508325</td>
          <td>0.120319</td>
          <td>4.373799e+09</td>
          <td>1.939409e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>19</th>
          <td>24.584273</td>
          <td>0.636252</td>
          <td>4.338260e+09</td>
          <td>4.077425e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>20</th>
          <td>251.397591</td>
          <td>0.778559</td>
          <td>4.188568e+09</td>
          <td>5.923020e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>21</th>
          <td>302.012549</td>
          <td>0.772594</td>
          <td>4.617226e+09</td>
          <td>5.055961e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>22</th>
          <td>18.579230</td>
          <td>0.256936</td>
          <td>4.290047e+09</td>
          <td>2.381188e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>23</th>
          <td>14.957312</td>
          <td>0.700940</td>
          <td>4.302504e+09</td>
          <td>4.249455e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>24</th>
          <td>212.302359</td>
          <td>0.646037</td>
          <td>3.510443e+09</td>
          <td>5.654748e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>25</th>
          <td>115.506297</td>
          <td>0.197162</td>
          <td>3.972348e+09</td>
          <td>3.077225e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>26</th>
          <td>257.504799</td>
          <td>0.581287</td>
          <td>4.235221e+09</td>
          <td>4.840406e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>27</th>
          <td>88.075640</td>
          <td>0.809222</td>
          <td>4.367557e+09</td>
          <td>5.851124e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>28</th>
          <td>183.278163</td>
          <td>0.741978</td>
          <td>3.186143e+09</td>
          <td>6.270258e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>29</th>
          <td>164.797852</td>
          <td>0.341820</td>
          <td>3.349166e+09</td>
          <td>4.071505e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>30</th>
          <td>295.145998</td>
          <td>0.781863</td>
          <td>4.612272e+09</td>
          <td>5.191414e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>31</th>
          <td>2.860968</td>
          <td>0.753246</td>
          <td>4.287888e+09</td>
          <td>4.410980e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>32</th>
          <td>280.331651</td>
          <td>0.618450</td>
          <td>4.480270e+09</td>
          <td>4.682645e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>33</th>
          <td>283.899973</td>
          <td>0.699313</td>
          <td>4.533249e+09</td>
          <td>5.019736e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>34</th>
          <td>266.522936</td>
          <td>0.407446</td>
          <td>4.314406e+09</td>
          <td>3.875891e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>35</th>
          <td>17.843621</td>
          <td>0.115373</td>
          <td>4.329252e+09</td>
          <td>1.787127e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>36</th>
          <td>298.905998</td>
          <td>0.132525</td>
          <td>4.486058e+09</td>
          <td>2.177395e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>37</th>
          <td>125.376380</td>
          <td>0.172816</td>
          <td>3.864726e+09</td>
          <td>3.013039e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>38</th>
          <td>109.539891</td>
          <td>0.834102</td>
          <td>4.102117e+09</td>
          <td>6.326264e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>39</th>
          <td>37.087412</td>
          <td>0.294448</td>
          <td>4.349793e+09</td>
          <td>2.732459e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>40</th>
          <td>202.925823</td>
          <td>0.150535</td>
          <td>3.547252e+09</td>
          <td>2.985553e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>41</th>
          <td>73.714102</td>
          <td>0.321960</td>
          <td>4.329891e+09</td>
          <td>3.290820e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>42</th>
          <td>265.457728</td>
          <td>0.714138</td>
          <td>4.369641e+09</td>
          <td>5.371035e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>43</th>
          <td>222.357877</td>
          <td>0.677068</td>
          <td>3.675527e+09</td>
          <td>5.800385e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>44</th>
          <td>277.956637</td>
          <td>0.880495</td>
          <td>4.562398e+09</td>
          <td>5.955538e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>45</th>
          <td>87.852259</td>
          <td>0.120677</td>
          <td>4.257516e+09</td>
          <td>2.436282e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>46</th>
          <td>82.075521</td>
          <td>0.416412</td>
          <td>4.291953e+09</td>
          <td>3.830479e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>47</th>
          <td>98.638003</td>
          <td>0.502126</td>
          <td>4.159454e+09</td>
          <td>4.495173e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>48</th>
          <td>222.271467</td>
          <td>0.613171</td>
          <td>3.675789e+09</td>
          <td>5.454972e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>49</th>
          <td>188.976525</td>
          <td>0.123347</td>
          <td>3.454562e+09</td>
          <td>2.834408e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    optres = results.loc[
        results["pareto-optimal"] == True, :
    ]  # Get only the optimal results
    plt.plot(
        results["DistrictCooling:Facility"], results["DistrictHeating:Facility"], "x"
    )  # Plot all results in the background
    plt.plot(
        optres["DistrictCooling:Facility"], optres["DistrictHeating:Facility"], "ro"
    )  # Plot optimal results in red
    plt.xlabel("Cooling demand")
    plt.ylabel("Heating demand")




.. parsed-literal::

    Text(0, 0.5, 'Heating demand')




.. image:: BuildingOptimizationWithDask_files/BuildingOptimizationWithDask_13_1.png


Visualize the results
---------------------

.. code:: ipython3

    optres = optres.sort_values("DistrictCooling:Facility")  # Sort by the first objective
    optresplot = optres.drop(columns="violation")  # Remove the constraint violation column
    ax = optresplot.plot.bar(
        subplots=True, legend=None, figsize=(10, 10)
    )  # Plot the variable values of each of the optimal solutions


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:307: MatplotlibDeprecationWarning:
    The rowNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().rowspan.start instead.
      layout[ax.rowNum, ax.colNum] = ax.get_visible()
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:307: MatplotlibDeprecationWarning:
    The colNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().colspan.start instead.
      layout[ax.rowNum, ax.colNum] = ax.get_visible()
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:313: MatplotlibDeprecationWarning:
    The rowNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().rowspan.start instead.
      if not layout[ax.rowNum + 1, ax.colNum]:
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:313: MatplotlibDeprecationWarning:
    The colNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().colspan.start instead.
      if not layout[ax.rowNum + 1, ax.colNum]:



.. image:: BuildingOptimizationWithDask_files/BuildingOptimizationWithDask_15_1.png


.. code:: ipython3

    client.restart()


.. parsed-literal::

    distributed.nanny - WARNING - Restarting worker
    distributed.nanny - WARNING - Restarting worker
    distributed.nanny - WARNING - Restarting worker
    distributed.nanny - WARNING - Restarting worker




.. raw:: html

    <table style="border: 2px solid white;">
    <tr>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Client</h3>
    <ul style="text-align: left; list-style: none; margin: 0; padding: 0;">
      <li><b>Scheduler: </b>tcp://127.0.0.1:42772</li>
      <li><b>Dashboard: </b><a href='/user/peterrwilson99/proxy/8787/status' target='_blank'>/user/peterrwilson99/proxy/8787/status</a></li>
    </ul>
    </td>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Cluster</h3>
    <ul style="text-align: left; list-style:none; margin: 0; padding: 0;">
      <li><b>Workers: </b>4</li>
      <li><b>Cores: </b>16</li>
      <li><b>Memory: </b>68.72 GB</li>
    </ul>
    </td>
    </tr>
    </table>
