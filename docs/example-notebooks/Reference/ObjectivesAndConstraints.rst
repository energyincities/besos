import libraries

.. code:: ipython3

    from besos import eppy_funcs as ef
    from besos.eplus_funcs import print_available_outputs
    from besos.evaluator import EvaluatorEP
    from besos.objectives import MeterReader, clear_outputs
    from besos.optimizer import NSGAII
    from besos.parameters import FieldSelector, Parameter, RangeParameter, wwr
    from besos.problem import EPProblem, Problem

Objectives and Constraints
==========================

``Evaluators`` support two types of outputs: Objectives and Constraints.
These are both made using the ``MeterReader`` and ``VariableReader``
classes. The only difference is how they are used by the problem.

First we load the EnergyPlus example file, clear any output data and
define some parameters.

.. code:: ipython3

    building = ef.get_building()
    clear_outputs(building)
    inputs = [
        wwr(),
        Parameter(
            FieldSelector(
                class_name="Material",
                object_name="Mass NonRes Wall Insulation",
                field_name="Thickness",
            ),
            RangeParameter(0.01, 0.99),
        ),
    ]

Objectives and constraints can be specified in various ways. + The most
explicit is by calling the relevant constructor.

.. code:: ipython3

    objectives = [
        MeterReader(
            key_name="Electricity:Facility", class_name="Output:Meter", frequency="Hourly"
        )
    ]
    EPProblem(outputs=objectives)




.. parsed-literal::

    EPProblem(outputs=[MeterReader(class_name='Output:Meter', frequency='Hourly', func=<function sum_values at 0x7f68014b4dd0>, key_name='Electricity:Facility')], minimize_outputs=[True], converters={'outputs': <class 'besos.objectives.MeterReader'>, 'constraints': <class 'besos.objectives.MeterReader'>})



-  The most concise is a list of the ``key_names``.

The constructor has defaults, so we can often omit ``class_name`` and
``frequency``. A list of key names will be automatically be converted by
``EPProblem``. Meters and variables that do not have a ``frequency``
specified will default to any frequency that is already used for that
output, or if none is used yet then they will use Hourly.

.. code:: ipython3

    objectives = ["Electricity:Facility"]
    EPProblem(outputs=objectives)




.. parsed-literal::

    EPProblem(outputs=[MeterReader(class_name='Output:Meter', func=<function sum_values at 0x7f68014b4dd0>, key_name='Electricity:Facility')], minimize_outputs=[True], converters={'outputs': <class 'besos.objectives.MeterReader'>, 'constraints': <class 'besos.objectives.MeterReader'>})



-  Using ``Problem``

If we do not need the output-reading features of meters, we can use
``Problem`` instead of ``EPProblem``, and they will be converted to
``Objective`` objects which act as placeholders. ``EPProblem`` converts
them to ``Meter:Reader`` objects. Either of these conversions can be
overriden using the converters argument.

.. code:: ipython3

    objectives = ["any", "names", "work"]
    Problem(outputs=objectives)




.. parsed-literal::

    Problem(outputs=[Objective(name='any'), Objective(name='names'), Objective(name='work')], minimize_outputs=[True, True, True], converters={'outputs': <class 'besos.IO_Objects.Objective'>, 'constraints': <class 'besos.IO_Objects.Objective'>})



-  Specifying the aggregation function

The ``func`` argument is used define how to aggregate the individual
time series results. By default, all measurements are summed. If we
wanted to instead minimize the variance, we can write our own aggrgation
function. Here we define two electricity objectives, the first summing
the hourly values and the second taking the variance.

.. code:: ipython3

    def variance(result):
        return result.data["Value"].var()


    objectives = [
        MeterReader("Electricity:Facility", name="Electricity Usage"),
        MeterReader("Electricity:Facility", func=variance, name="Electricity Variance"),
    ]

When we want to specify the direction of optimisation, we can use
``minmize_outputs`` (defaults to ``true`` for all objectives). Here we
say we want to search for a design that has: + low electricty use
(minimize objective 1 defined above) + high variability of electricity
use (maximize objective 2 defined above) + less than 800 kgCO2
(constraint)

.. code:: ipython3

    evaluator = EvaluatorEP(
        EPProblem(
            inputs=inputs,
            outputs=objectives,
            minimize_outputs=[True, True],
            constraints=["CO2:Facility"],
            constraint_bounds=["<=800"],
        ),
        building,
        out_dir="outputdir",
    )

.. code:: ipython3

    # this cell runs the optimisation
    results1 = NSGAII(evaluator, evaluations=1, population_size=10)
    results1




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Window to Wall Ratio</th>
          <th>RangeParameter [0.01, 0.99]</th>
          <th>Electricity Usage</th>
          <th>Electricity Variance</th>
          <th>CO2:Facility</th>
          <th>violation</th>
          <th>pareto-optimal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.119989</td>
          <td>0.882785</td>
          <td>1.743436e+09</td>
          <td>6.988464e+14</td>
          <td>684.446725</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.585907</td>
          <td>0.287694</td>
          <td>1.762123e+09</td>
          <td>7.073158e+14</td>
          <td>695.797631</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.931292</td>
          <td>0.554707</td>
          <td>1.751476e+09</td>
          <td>7.021104e+14</td>
          <td>689.509729</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.969365</td>
          <td>0.011515</td>
          <td>1.830906e+09</td>
          <td>7.600471e+14</td>
          <td>780.888404</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.757057</td>
          <td>0.332694</td>
          <td>1.759230e+09</td>
          <td>7.058550e+14</td>
          <td>695.124565</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.489655</td>
          <td>0.284131</td>
          <td>1.762302e+09</td>
          <td>7.074258e+14</td>
          <td>695.944665</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.661926</td>
          <td>0.549753</td>
          <td>1.751542e+09</td>
          <td>7.021496e+14</td>
          <td>689.568055</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.811219</td>
          <td>0.691923</td>
          <td>1.748593e+09</td>
          <td>7.016378e+14</td>
          <td>686.908295</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.330034</td>
          <td>0.464210</td>
          <td>1.754723e+09</td>
          <td>7.039286e+14</td>
          <td>690.890998</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.778857</td>
          <td>0.132968</td>
          <td>1.775151e+09</td>
          <td>7.142855e+14</td>
          <td>707.998154</td>
          <td>0</td>
          <td>False</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    results1.describe()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Window to Wall Ratio</th>
          <th>RangeParameter [0.01, 0.99]</th>
          <th>Electricity Usage</th>
          <th>Electricity Variance</th>
          <th>CO2:Facility</th>
          <th>violation</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>count</th>
          <td>10.000000</td>
          <td>10.000000</td>
          <td>1.000000e+01</td>
          <td>1.000000e+01</td>
          <td>10.000000</td>
          <td>10.0</td>
        </tr>
        <tr>
          <th>mean</th>
          <td>0.643530</td>
          <td>0.419238</td>
          <td>1.763948e+09</td>
          <td>7.103602e+14</td>
          <td>701.707722</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>std</th>
          <td>0.268330</td>
          <td>0.262017</td>
          <td>2.514026e+07</td>
          <td>1.797485e+13</td>
          <td>28.578856</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>min</th>
          <td>0.119989</td>
          <td>0.011515</td>
          <td>1.743436e+09</td>
          <td>6.988464e+14</td>
          <td>684.446725</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>25%</th>
          <td>0.513718</td>
          <td>0.285022</td>
          <td>1.751493e+09</td>
          <td>7.021202e+14</td>
          <td>689.524310</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>50%</th>
          <td>0.709492</td>
          <td>0.398452</td>
          <td>1.756976e+09</td>
          <td>7.048918e+14</td>
          <td>693.007782</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>75%</th>
          <td>0.803129</td>
          <td>0.553469</td>
          <td>1.762257e+09</td>
          <td>7.073983e+14</td>
          <td>695.907907</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>max</th>
          <td>0.969365</td>
          <td>0.882785</td>
          <td>1.830906e+09</td>
          <td>7.600471e+14</td>
          <td>780.888404</td>
          <td>0.0</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # this cell runs the optimisation
    results2 = NSGAII(evaluator, evaluations=10, population_size=10)
    results2




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Window to Wall Ratio</th>
          <th>RangeParameter [0.01, 0.99]</th>
          <th>Electricity Usage</th>
          <th>Electricity Variance</th>
          <th>CO2:Facility</th>
          <th>violation</th>
          <th>pareto-optimal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.416852</td>
          <td>0.853750</td>
          <td>1.744062e+09</td>
          <td>6.988618e+14</td>
          <td>685.337017</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.027703</td>
          <td>0.669789</td>
          <td>1.748995e+09</td>
          <td>7.018551e+14</td>
          <td>687.160633</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.686940</td>
          <td>0.903360</td>
          <td>1.745100e+09</td>
          <td>7.001653e+14</td>
          <td>685.100750</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.944074</td>
          <td>0.429213</td>
          <td>1.756518e+09</td>
          <td>7.046349e+14</td>
          <td>691.612061</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.656314</td>
          <td>0.908240</td>
          <td>1.745160e+09</td>
          <td>7.002909e+14</td>
          <td>685.101769</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.762704</td>
          <td>0.848589</td>
          <td>1.744128e+09</td>
          <td>6.989036e+14</td>
          <td>685.377176</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.845933</td>
          <td>0.928448</td>
          <td>1.745011e+09</td>
          <td>7.002508e+14</td>
          <td>684.991962</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.950455</td>
          <td>0.055710</td>
          <td>1.793358e+09</td>
          <td>7.273931e+14</td>
          <td>729.680123</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.713841</td>
          <td>0.444785</td>
          <td>1.755086e+09</td>
          <td>7.041377e+14</td>
          <td>691.220545</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.986133</td>
          <td>0.056327</td>
          <td>1.793116e+09</td>
          <td>7.272010e+14</td>
          <td>729.358551</td>
          <td>0</td>
          <td>False</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    results2.describe()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Window to Wall Ratio</th>
          <th>RangeParameter [0.01, 0.99]</th>
          <th>Electricity Usage</th>
          <th>Electricity Variance</th>
          <th>CO2:Facility</th>
          <th>violation</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>count</th>
          <td>10.000000</td>
          <td>10.000000</td>
          <td>1.000000e+01</td>
          <td>1.000000e+01</td>
          <td>10.000000</td>
          <td>10.0</td>
        </tr>
        <tr>
          <th>mean</th>
          <td>0.699095</td>
          <td>0.609821</td>
          <td>1.757053e+09</td>
          <td>7.063694e+14</td>
          <td>695.494059</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>std</th>
          <td>0.291322</td>
          <td>0.344819</td>
          <td>1.958696e+07</td>
          <td>1.120168e+13</td>
          <td>18.104944</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>min</th>
          <td>0.027703</td>
          <td>0.055710</td>
          <td>1.744062e+09</td>
          <td>6.988618e+14</td>
          <td>684.991962</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>25%</th>
          <td>0.663970</td>
          <td>0.433106</td>
          <td>1.745033e+09</td>
          <td>7.001867e+14</td>
          <td>685.160581</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>50%</th>
          <td>0.738273</td>
          <td>0.759189</td>
          <td>1.747077e+09</td>
          <td>7.010730e+14</td>
          <td>686.268905</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>75%</th>
          <td>0.919539</td>
          <td>0.890958</td>
          <td>1.756160e+09</td>
          <td>7.045106e+14</td>
          <td>691.514182</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>max</th>
          <td>0.986133</td>
          <td>0.928448</td>
          <td>1.793358e+09</td>
          <td>7.273931e+14</td>
          <td>729.680123</td>
          <td>0.0</td>
        </tr>
      </tbody>
    </table>
    </div>



Get available objectives
------------------------

The user can use print\_available\_outputs to print out the available
objectives for this building

.. code:: ipython3


    building = ef.get_building(mode="idf")
    print_available_outputs(building, name="facility", frequency="monthly")


.. parsed-literal::

    ['Electricity:Facility', 'Monthly']
    ['Gas:Facility', 'Monthly']
    ['CO2:Facility', 'Monthly']
    ['CO:Facility', 'Monthly']
    ['CH4:Facility', 'Monthly']
    ['NOx:Facility', 'Monthly']
    ['N2O:Facility', 'Monthly']
    ['SO2:Facility', 'Monthly']
    ['PM:Facility', 'Monthly']
    ['PM10:Facility', 'Monthly']
    ['PM2.5:Facility', 'Monthly']
    ['NH3:Facility', 'Monthly']
    ['NMVOC:Facility', 'Monthly']
    ['Hg:Facility', 'Monthly']
    ['Pb:Facility', 'Monthly']
    ['WaterEnvironmentalFactors:Facility', 'Monthly']
    ['Nuclear High:Facility', 'Monthly']
    ['Nuclear Low:Facility', 'Monthly']
    ['Carbon Equivalent:Facility', 'Monthly']
