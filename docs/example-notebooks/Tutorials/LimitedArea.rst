Rooftop constraint
==================

This notebook incorporates a roof area constraint: The total area of all
the converters occupying roof area must be less than the total area
available on the roof.

.. code:: ipython3

    from pyehub.energy_hub.ehub_model import EHubModel
    from pyehub.energy_hub.utils import constraint
    from pyehub.outputter import pretty_print

    from pylp import RealVariable

-  Specify the available roof area.
-  Inherit the ``EHubModel`` class to build a custom model.
-  Specify the Roof-Area constraint in this new model:

   -  precede the roof-area constraint function by ``@constraint()``
      decorator. *[This is how the model recognizes a constraint]*
   -  make ``roof_area_per_unit_capacity``, which is a ``dictionary``
      containing the area occupied by roof-cnverters per unit capacity
      of the tech.
   -  calculate ``total_roof_area`` occupied by all roof-converters by
      summing up products of ``roof_area_per_unit_capacity`` and
      ``capacities`` ranging for all roof-converters.
   -  Impose the Roof-Area constraint.

.. code:: ipython3

    MAX_ROOF_AREA = 80  # available roof area


    class RoofModel(EHubModel):
        """
        This is a subclass of EHubModel with roof constraints.
        """

        @constraint()
        def roof_tech_area_below_max(self):
            """ Ensure the roof techs are taking up less area than there is roof.
            """
            # The following dictionary contains the 'area [m2] per unit capacity [kWp]'
            roof_area_per_unit_capacity = {"PV": 40, "ST": 5}

            # multiply roof-area-per-unit-capacity by the capacity for all listed technologies and sum
            total_roof_area = sum(
                (self.capacities[tech_name]) * (roof_area_per_unit_capacity[tech_name])
                for tech_name in roof_area_per_unit_capacity
            )

            # return the constraint that this variable <= available area
            return total_roof_area <= MAX_ROOF_AREA

Now we load and run a model.

.. code:: ipython3

    excel_file = "test_file_all_constraints_work.xlsx"  # name of the excel file. [This must be in the current directory]
    my_model = RoofModel(
        excel=excel_file
    )  # instantiate our model. Nothing is solved at this point.
    results = my_model.solve()  # solve the model and get back our results
    pretty_print(results)  # print the results to the console


.. parsed-literal::

    Version: 0.1.0
    Solver
    	termination condition: Optimal
    	time: 0.06752891279757023
    Solution

    ========== Stuff ==========

    ANNUAL_MAINTENANCE_STORAGE_COSTS:
                    ANNUAL_MAINTENANCE_STORAGE_COSTS
    Battery                                        0
    Hot Water Tank                                 0

    BIG_M:
    99999

    Battery:
    0.0

    Boiler:
    10.0

    CARBON_CREDITS:
                CARBON_CREDITS
    Elec
    Gas
    Grid
    Heat
    Irradiation
    PV_Elec                0.3

    CARBON_FACTORS:
                CARBON_FACTORS
    Elec
    Gas                  0.194
    Grid                  0.35
    Heat
    Irradiation
    PV_Elec

    CHARGING_EFFICIENCY:
                    CHARGING_EFFICIENCY
    Battery                        0.99
    Hot Water Tank                 0.99

    CHP:
    5.0

    CONVERSION_EFFICIENCY:
              Elec  ...  PV_Elec
    Boiler     0.0  ...    0.000
    CHP        0.3  ...    0.000
    GSHP      -1.0  ...    0.000
    Grid       1.0  ...    0.000
    HP        -1.0  ...    0.000
    MicroCHP   0.9  ...    0.000
    PV         0.0  ...    0.165
    ST         0.0  ...    0.000

    [8 rows x 6 columns]

    DISCHARGING_EFFICIENCY:
                    DISCHARGING_EFFICIENCY
    Battery                           0.99
    Hot Water Tank                    0.99

    FEED_IN_TARIFFS:
             FEED_IN_TARIFFS
    PV_Elec              4.0

    FIXED_CAPITAL_COSTS:
              FIXED_CAPITAL_COSTS
    Boiler                      0
    CHP                         0
    GSHP                        0
    Grid                        0
    HP                          0
    MicroCHP                    0
    PV                          0
    ST                          0

    FIXED_CAPITAL_COSTS_STORAGE:
                    FIXED_CAPITAL_COSTS_STORAGE
    Battery                                   0
    Hot Water Tank                            0

    FUEL_PRICES:
                FUEL_PRICES
    Elec
    Gas               0.028
    Grid               0.13
    Heat
    Irradiation
    PV_Elec

    GSHP:
    0.0

    Grid:
    2.68114

    HP:
    0.0

    Hot Water Tank:
    21.0085

    LINEAR_CAPITAL_COSTS:
              LINEAR_CAPITAL_COSTS
    Boiler                   500.0
    CHP                     1000.0
    GSHP                   15000.0
    Grid                       0.0
    HP                      1500.0
    MicroCHP                1800.0
    PV                         0.1
    ST                      4500.0

    LINEAR_STORAGE_COSTS:
                    LINEAR_STORAGE_COSTS
    Battery                       800.00
    Hot Water Tank                  1.33

    LOADS:
        Elec  Heat
    0    1.0  20.0
    1    4.0  20.0
    2    4.0  20.0
    3    4.0  20.0
    4    4.0  20.0
    5    4.0  20.0
    6    4.0  20.0
    7    4.0  12.0
    8    4.0  12.0
    9    4.0  12.0
    10   4.0  12.0

    MAX_CARBON:
    None

    MAX_CHARGE_RATE:
                    MAX_CHARGE_RATE
    Battery                     0.3
    Hot Water Tank              0.3

    MAX_DISCHARGE_RATE:
                    MAX_DISCHARGE_RATE
    Battery                        0.3
    Hot Water Tank                 0.3

    MIN_STATE_OF_CHARGE:
                    MIN_STATE_OF_CHARGE
    Battery                         0.0
    Hot Water Tank                  0.0

    MicroCHP:
    1.4654

    NET_PRESENT_VALUE_STORAGE:
                    NET_PRESENT_VALUE_STORAGE
    Battery                          0.101852
    Hot Water Tank                   0.101852

    NET_PRESENT_VALUE_TECH:
              NET_PRESENT_VALUE_TECH
    Boiler                    0.0888
    CHP                       0.1019
    GSHP                      0.0817
    Grid                      0.0800
    HP                        0.1019
    MicroCHP                  0.1019
    PV                        0.1019
    ST                        0.0858

    OMV_COSTS:
              OMV_COSTS
    Boiler         0.01
    CHP            0.03
    GSHP           0.10
    Grid           0.00
    HP             0.10
    MicroCHP       0.02
    PV             0.00
    ST             0.01

    PART_LOAD:
              Elec  Heat
    CHP        0.5   NaN
    MicroCHP   0.5   0.5

    PV:
    2.0

    ST:
    0.0

    STORAGE_STANDING_LOSSES:
                    STORAGE_STANDING_LOSSES
    Battery                           0.001
    Hot Water Tank                    0.001

    TIME_SERIES:
        Elec  Heat  Irradiation
    0    1.0  20.0        0.000
    1    4.0  20.0        0.019
    2    4.0  20.0        0.052
    3    4.0  20.0        0.092
    4    4.0  20.0        0.115
    5    4.0  20.0        0.116
    6    4.0  20.0        0.095
    7    4.0  12.0        0.057
    8    4.0  12.0        0.006
    9    4.0  12.0        0.000
    10   4.0  12.0        0.000

    capacities:
             capacities
    Boiler       Boiler
    CHP             CHP
    GSHP           GSHP
    Grid           Grid
    HP               HP
    MicroCHP   MicroCHP
    PV               PV
    ST               ST

    capacity_storage:
                    capacity_storage
    Battery                   0.0000
    Hot Water Tank           21.0085

    capacity_tech:
              capacity_tech
    Boiler         10.00000
    CHP             5.00000
    GSHP            0.00000
    Grid            2.68114
    HP              0.00000
    MicroCHP        1.46540
    PV              2.00000
    ST              0.00000

    demands:
    ['Elec', 'Heat']

    energy_exported:
        PV_Elec
    0   0.00000
    1   0.00627
    2   0.01716
    3   0.03036
    4   0.03795
    5   0.03828
    6   0.03135
    7   0.01881
    8   0.00198
    9   0.00000
    10  0.00000

    energy_from_storage:
        Battery  Hot Water Tank
    0       0.0         4.60000
    1       0.0         2.68685
    2       0.0         2.68685
    3       0.0         2.68685
    4       0.0         2.68685
    5       0.0         2.68685
    6       0.0         2.68685
    7       0.0         0.00000
    8       0.0         0.00000
    9       0.0         0.00000
    10      0.0         0.00000

    energy_imported:
            Gas     Grid
    0   11.1111  0.00000
    1   11.4654  2.68114
    2   11.4654  2.68114
    3   11.4654  2.68114
    4   11.4654  2.68114
    5   11.4654  2.68114
    6   11.4654  2.68114
    7   11.4654  2.68114
    8   11.4654  2.68114
    9   11.4654  2.68114
    10  11.4654  2.68114

    energy_input:
        Boiler  ...   ST
    0     10.0  ...  0.0
    1     10.0  ...  0.0
    2     10.0  ...  0.0
    3     10.0  ...  0.0
    4     10.0  ...  0.0
    5     10.0  ...  0.0
    6     10.0  ...  0.0
    7     10.0  ...  0.0
    8     10.0  ...  0.0
    9     10.0  ...  0.0
    10    10.0  ...  0.0

    [11 rows x 8 columns]

    energy_to_storage:
        Battery  Hot Water Tank
    0       0.0         0.00000
    1       0.0         0.00000
    2       0.0         0.00000
    3       0.0         0.00000
    4       0.0         0.00000
    5       0.0         0.00000
    6       0.0         0.00000
    7       0.0         5.31315
    8       0.0         5.31315
    9       0.0         5.31315
    10      0.0         5.31315

    export_streams:
    ['PV_Elec']

    import_streams:
    ['Grid', 'Gas']

    investment_cost:
    1225.15

    is_installed:
              is_installed
    Boiler               1
    CHP                  1
    GSHP                 0
    Grid                 1
    HP                   0
    MicroCHP             1
    PV                   1
    ST                   0

    is_installed_2:
                    is_installed_2
    Battery                      0
    Hot Water Tank               1

    is_on:
        CHP  MicroCHP
    0     0         1
    1     0         1
    2     0         1
    3     0         1
    4     0         1
    5     0         1
    6     0         1
    7     0         1
    8     0         1
    9     0         1
    10    0         1

    maintenance_cost:
    3.0204

    operating_cost:
    6.27827

    output_streams:
    ['Elec', 'Heat', 'PV_Elec']

    part_load:
    ['MicroCHP', 'CHP']

    sources:
    ['Irradiation']

    storage_capacity:
                   storage_capacity
    Battery                 Battery
    Hot Water Tank   Hot Water Tank

    storage_level:
        Battery  Hot Water Tank
    0       0.0        21.00850
    1       0.0        16.34110
    2       0.0        13.61070
    3       0.0        10.88310
    4       0.0         8.15827
    5       0.0         5.43613
    6       0.0         2.71670
    7       0.0         0.00000
    8       0.0         5.26002
    9       0.0        10.51480
    10      0.0        15.76430
    11      0.0        21.00850

    storages:
    ['Battery', 'Hot Water Tank']

    stream_timeseries:
                stream_timeseries
    Irradiation       Irradiation

    streams:
    ['Elec', 'Heat', 'Irradiation', 'Grid', 'Gas', 'PV_Elec']

    technologies:
    ['Grid', 'HP', 'Boiler', 'MicroCHP', 'PV', 'ST', 'CHP', 'GSHP']

    time:
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    total_carbon:
    33.7278

    total_cost:
    1234.45


Tracking the roof area
----------------------

To keep track of the roof area used we build a tracking constraint that
will track the area. Then add an additional constraint to contrain the
tracking constraint.

By adding a variable to track the roof area it will be stored in the
output file.

.. code:: ipython3

    MAX_ROOF_AREA = 80  # available roof area


    class RoofModel(EHubModel):
        """
        This is a subclass of EHubModel with roof constraints.
        """

        def __init__(
            self, *, excel=None, request=None, max_carbon=0, MAX_ROOF_AREA=MAX_ROOF_AREA
        ):
            super().__init__(excel=excel, request=request)
            self.MAX_ROOF_AREA = MAX_ROOF_AREA
            # The following dictionary contains the 'area [m2] per unit capacity [kWp]'
            self.area_per_capacity = {"PV": 40, "ST": 5}

        def _add_variables(self):
            super()._add_variables()
            self.total_roof_area = RealVariable()

        # This will track the amount of are used
        @constraint()
        def roof_tech_area_definition(self):
            """
            Ensure the roof techs are taking up less area than there is roof.
            """

            # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
            total_roof_area = sum(
                (self.capacities[tech_name]) * (self.area_per_capacity[tech_name])
                for tech_name in self.area_per_capacity
            )

            return self.total_roof_area == total_roof_area

        # This will constrain the area used.
        @constraint()
        def roof_tech_area_below_max(self):
            """ Ensure the roof techs are taking up less area than there is roof.
            """

            # return the constraint that this variable <= available area
            return self.total_roof_area <= self.MAX_ROOF_AREA

.. code:: ipython3

    excel_file = "test_file_all_constraints_work.xlsx"  # name of the excel file. [This must be in the current directory]
    my_model = RoofModel(
        excel=excel_file
    )  # instantiate our model. Nothing is solved at this point.
    results = my_model.solve()  # solve the model and get back our results
    pretty_print(results)  # print the results to the console


.. parsed-literal::

    Version: 0.1.0
    Solver
    	termination condition: Optimal
    	time: 0.062328217551112175
    Solution

    ========== Stuff ==========

    ANNUAL_MAINTENANCE_STORAGE_COSTS:
                    ANNUAL_MAINTENANCE_STORAGE_COSTS
    Battery                                        0
    Hot Water Tank                                 0

    BIG_M:
    99999

    Battery:
    0.0

    Boiler:
    10.0

    CARBON_CREDITS:
                CARBON_CREDITS
    Elec
    Gas
    Grid
    Heat
    Irradiation
    PV_Elec                0.3

    CARBON_FACTORS:
                CARBON_FACTORS
    Elec
    Gas                  0.194
    Grid                  0.35
    Heat
    Irradiation
    PV_Elec

    CHARGING_EFFICIENCY:
                    CHARGING_EFFICIENCY
    Battery                        0.99
    Hot Water Tank                 0.99

    CHP:
    5.0

    CONVERSION_EFFICIENCY:
              Elec  ...  PV_Elec
    Boiler     0.0  ...    0.000
    CHP        0.3  ...    0.000
    GSHP      -1.0  ...    0.000
    Grid       1.0  ...    0.000
    HP        -1.0  ...    0.000
    MicroCHP   0.9  ...    0.000
    PV         0.0  ...    0.165
    ST         0.0  ...    0.000

    [8 rows x 6 columns]

    DISCHARGING_EFFICIENCY:
                    DISCHARGING_EFFICIENCY
    Battery                           0.99
    Hot Water Tank                    0.99

    FEED_IN_TARIFFS:
             FEED_IN_TARIFFS
    PV_Elec              4.0

    FIXED_CAPITAL_COSTS:
              FIXED_CAPITAL_COSTS
    Boiler                      0
    CHP                         0
    GSHP                        0
    Grid                        0
    HP                          0
    MicroCHP                    0
    PV                          0
    ST                          0

    FIXED_CAPITAL_COSTS_STORAGE:
                    FIXED_CAPITAL_COSTS_STORAGE
    Battery                                   0
    Hot Water Tank                            0

    FUEL_PRICES:
                FUEL_PRICES
    Elec
    Gas               0.028
    Grid               0.13
    Heat
    Irradiation
    PV_Elec

    GSHP:
    0.0

    Grid:
    2.68114

    HP:
    0.0

    Hot Water Tank:
    21.0085

    LINEAR_CAPITAL_COSTS:
              LINEAR_CAPITAL_COSTS
    Boiler                   500.0
    CHP                     1000.0
    GSHP                   15000.0
    Grid                       0.0
    HP                      1500.0
    MicroCHP                1800.0
    PV                         0.1
    ST                      4500.0

    LINEAR_STORAGE_COSTS:
                    LINEAR_STORAGE_COSTS
    Battery                       800.00
    Hot Water Tank                  1.33

    LOADS:
        Elec  Heat
    0    1.0  20.0
    1    4.0  20.0
    2    4.0  20.0
    3    4.0  20.0
    4    4.0  20.0
    5    4.0  20.0
    6    4.0  20.0
    7    4.0  12.0
    8    4.0  12.0
    9    4.0  12.0
    10   4.0  12.0

    MAX_CARBON:
    None

    MAX_CHARGE_RATE:
                    MAX_CHARGE_RATE
    Battery                     0.3
    Hot Water Tank              0.3

    MAX_DISCHARGE_RATE:
                    MAX_DISCHARGE_RATE
    Battery                        0.3
    Hot Water Tank                 0.3

    MAX_ROOF_AREA:
    80

    MIN_STATE_OF_CHARGE:
                    MIN_STATE_OF_CHARGE
    Battery                         0.0
    Hot Water Tank                  0.0

    MicroCHP:
    1.4654

    NET_PRESENT_VALUE_STORAGE:
                    NET_PRESENT_VALUE_STORAGE
    Battery                          0.101852
    Hot Water Tank                   0.101852

    NET_PRESENT_VALUE_TECH:
              NET_PRESENT_VALUE_TECH
    Boiler                    0.0888
    CHP                       0.1019
    GSHP                      0.0817
    Grid                      0.0800
    HP                        0.1019
    MicroCHP                  0.1019
    PV                        0.1019
    ST                        0.0858

    OMV_COSTS:
              OMV_COSTS
    Boiler         0.01
    CHP            0.03
    GSHP           0.10
    Grid           0.00
    HP             0.10
    MicroCHP       0.02
    PV             0.00
    ST             0.01

    PART_LOAD:
              Elec  Heat
    CHP        0.5   NaN
    MicroCHP   0.5   0.5

    PV:
    2.0

    ST:
    0.0

    STORAGE_STANDING_LOSSES:
                    STORAGE_STANDING_LOSSES
    Battery                           0.001
    Hot Water Tank                    0.001

    TIME_SERIES:
        Elec  Heat  Irradiation
    0    1.0  20.0        0.000
    1    4.0  20.0        0.019
    2    4.0  20.0        0.052
    3    4.0  20.0        0.092
    4    4.0  20.0        0.115
    5    4.0  20.0        0.116
    6    4.0  20.0        0.095
    7    4.0  12.0        0.057
    8    4.0  12.0        0.006
    9    4.0  12.0        0.000
    10   4.0  12.0        0.000

    area_per_capacity:
        area_per_capacity
    PV                 40
    ST                  5

    capacities:
             capacities
    Boiler       Boiler
    CHP             CHP
    GSHP           GSHP
    Grid           Grid
    HP               HP
    MicroCHP   MicroCHP
    PV               PV
    ST               ST

    capacity_storage:
                    capacity_storage
    Battery                   0.0000
    Hot Water Tank           21.0085

    capacity_tech:
              capacity_tech
    Boiler         10.00000
    CHP             5.00000
    GSHP            0.00000
    Grid            2.68114
    HP              0.00000
    MicroCHP        1.46540
    PV              2.00000
    ST              0.00000

    demands:
    ['Elec', 'Heat']

    energy_exported:
        PV_Elec
    0   0.00000
    1   0.00627
    2   0.01716
    3   0.03036
    4   0.03795
    5   0.03828
    6   0.03135
    7   0.01881
    8   0.00198
    9   0.00000
    10  0.00000

    energy_from_storage:
        Battery  Hot Water Tank
    0       0.0         4.60000
    1       0.0         2.68685
    2       0.0         2.68685
    3       0.0         2.68685
    4       0.0         2.68685
    5       0.0         2.68685
    6       0.0         2.68685
    7       0.0         0.00000
    8       0.0         0.00000
    9       0.0         0.00000
    10      0.0         0.00000

    energy_imported:
            Gas     Grid
    0   11.1111  0.00000
    1   11.4654  2.68114
    2   11.4654  2.68114
    3   11.4654  2.68114
    4   11.4654  2.68114
    5   11.4654  2.68114
    6   11.4654  2.68114
    7   11.4654  2.68114
    8   11.4654  2.68114
    9   11.4654  2.68114
    10  11.4654  2.68114

    energy_input:
        Boiler  ...   ST
    0     10.0  ...  0.0
    1     10.0  ...  0.0
    2     10.0  ...  0.0
    3     10.0  ...  0.0
    4     10.0  ...  0.0
    5     10.0  ...  0.0
    6     10.0  ...  0.0
    7     10.0  ...  0.0
    8     10.0  ...  0.0
    9     10.0  ...  0.0
    10    10.0  ...  0.0

    [11 rows x 8 columns]

    energy_to_storage:
        Battery  Hot Water Tank
    0       0.0         0.00000
    1       0.0         0.00000
    2       0.0         0.00000
    3       0.0         0.00000
    4       0.0         0.00000
    5       0.0         0.00000
    6       0.0         0.00000
    7       0.0         5.31315
    8       0.0         5.31315
    9       0.0         5.31315
    10      0.0         5.31315

    export_streams:
    ['PV_Elec']

    import_streams:
    ['Grid', 'Gas']

    investment_cost:
    1225.15

    is_installed:
              is_installed
    Boiler               1
    CHP                  1
    GSHP                 0
    Grid                 1
    HP                   0
    MicroCHP             1
    PV                   1
    ST                   0

    is_installed_2:
                    is_installed_2
    Battery                      0
    Hot Water Tank               1

    is_on:
        CHP  MicroCHP
    0     0         1
    1     0         1
    2     0         1
    3     0         1
    4     0         1
    5     0         1
    6     0         1
    7     0         1
    8     0         1
    9     0         1
    10    0         1

    maintenance_cost:
    3.0204

    operating_cost:
    6.27827

    output_streams:
    ['Elec', 'Heat', 'PV_Elec']

    part_load:
    ['MicroCHP', 'CHP']

    sources:
    ['Irradiation']

    storage_capacity:
                   storage_capacity
    Battery                 Battery
    Hot Water Tank   Hot Water Tank

    storage_level:
        Battery  Hot Water Tank
    0       0.0        21.00850
    1       0.0        16.34110
    2       0.0        13.61070
    3       0.0        10.88310
    4       0.0         8.15827
    5       0.0         5.43613
    6       0.0         2.71670
    7       0.0         0.00000
    8       0.0         5.26002
    9       0.0        10.51480
    10      0.0        15.76430
    11      0.0        21.00850

    storages:
    ['Battery', 'Hot Water Tank']

    stream_timeseries:
                stream_timeseries
    Irradiation       Irradiation

    streams:
    ['Elec', 'Heat', 'Irradiation', 'Grid', 'Gas', 'PV_Elec']

    technologies:
    ['Grid', 'HP', 'Boiler', 'MicroCHP', 'PV', 'ST', 'CHP', 'GSHP']

    time:
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    total_carbon:
    33.7278

    total_cost:
    1234.45

    total_roof_area:
    80.0
