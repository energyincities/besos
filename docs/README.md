collect_rst.py will convert referenced notebooks to rst's for readthedocs, reference
notebooks are in rtd-notebooks.txt. collect_rst.py will detect changes, whenever a change
to the notebooks repo has occured, run './collect_rst.py --update' to execute and save
new notebook rst file. If change has occured without running './collect_rst.py --update',
pipeline will fail. The py file does not grab images, so if rtd is built and missing images,
go into notebook find which image is needed, place in the notebooks directory in an 'image' folder.
The 'python collect_rst' command will not fail if unnecessary notebook files exist within the 'reference-nbs'
or 'example-notebooks' directories, however if the update command is run all unnecessary notebook files within
both directories will be removed.

NOTE: 'rtd-notebooks.txt' output directories must exist as currently collect_rst.py will
not create folders
NOTE: Only compares markdown and code cells, not output cells, therefore changes to
outputs will not be updated unless code cells are altered as well.
NOTE: To force update on a notebook, remove the rst file from 'reference-nbs', then
run './collect_rst --update'


collect_rst logic:

- Clones besos-examples repo
- Reads 'rtd-notebooks.txt' and finds paths to notebooks for readthedocs and saves where rst outputs will be
- get_diff() compares 'reference-nbs' and 'besos-examples' ipynbs, returns paths to all diff 'besos-examples' ipynbs
- if no '--update' arg, checks if return length is 0 and completes, if not it throws an error saying to update files
- if '--update' arg, checks to see if there are any unnecessary files (not in 'rtd-notebooks.txt') in 'reference-nbs' and 'example-notebooks' directories and removes them
- Copies get_diff() unexecuted ipynbs from 'besos-examples' into 'reference-nbs'
- Executes all diff notebooks from get_diff() in 'besos-examples', converts them to rsts, and saves them in their respective out_dirs specified in 'rtd-notebooks.txt'
- Removes 'besos-examples' repo
