#!/usr/bin/env python3
import subprocess
from pathlib import Path
import os
import sys
import shutil
import glob
import json
from besos.besos_utils import get_file_name

# TODO: Add functionality for transferring images referenced in notebook


def remove_whitespace(path):
    """
    nbconvert files leave many whitespace characters and an excess of newlines at end of files, this removes them to pass pre-commits
    """
    f = open(path, "r")
    lines = []
    for line in f:
        lines.append((line.rstrip() + "\n"))
    f.close()
    for line in reversed(list(lines)):  # this loop removes eof new lines
        if len(line) == 1 and len(lines) > 1:
            lines = lines[:-1]
        else:
            break
    f = open(path, "w")
    for line in lines:
        f.write(line)
    f.close()


def fix_broken_links():
    """
    Enters each rst file and ensures links are correct for local and gitlab references
    """
    besos_examples_link = (
        "https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/"
    )
    rsts = glob.glob("**/*.rst", recursive=True)  # rst files to loop over
    examples_path = os.path.join("besos-examples", "besos", "examples")
    notebooks = glob.glob(
        os.path.join(examples_path, "**", "*.ipynb"), recursive=True
    ) + glob.glob(
        os.path.join(examples_path, "**", "*.py"), recursive=True
    )  # contains py and ipynb files
    notebooks = [path.split("besos/examples/")[1] for path in list(notebooks)]
    notebook_names = [get_file_name(rst_file).replace(".rst", "") for rst_file in rsts]
    docs_paths = [
        path.replace(".rst", ".html") for path in rsts
    ]  # for local references

    broken_links = []
    for rst in rsts:  # loops over all rsts in docs
        ipynb_links = []
        py_links = []
        html_links = []
        gitlab_links = []
        other_links = []
        html_depth = len(rst.split("/")) - 1
        readObj = open(rst, "r")
        htmlFlag = False  # flag to ignore html section of rst files while finding links
        for line in readObj:  # loops over each line
            if (not line.startswith("    ")) and (line != "\n"):
                htmlFlag = False
            if line.startswith(".. "):
                htmlFlag = True
            if ("<" in line) and not htmlFlag:
                for link in line.split("<"):
                    if ">" in link and not link.startswith("#"):
                        link_final = link.split(">")[0]
                        if ("gitlab.com" in link_final) or (
                            "besos.readthedocs" in link_final
                        ):
                            gitlab_links.append((line, link_final))
                        elif "https" not in link_final:
                            if link_final.endswith(".ipynb"):
                                ipynb_links.append((line, link_final))
                            elif link_final.endswith(".py"):
                                py_links.append((line, link_final))
                            elif link_final.endswith(".html"):
                                html_links.append((line, link_final))
                        else:
                            other_links.append((line, link_final))

        link_tuples = (
            {}
        )  # link dictionary with the key set to the line, and the data being a tuple with (orig_link,new_link)

        for line, link in gitlab_links:
            link = link.replace("%20", " ")
            for path in docs_paths:
                if get_file_name(path) == get_file_name(link).replace(
                    ".ipynb", ".html"
                ):
                    path = "../" * html_depth + path
                    try:
                        link_tuples[line] = link_tuples[line].append(
                            (link.replace(" ", "%20"), path)
                        )
                    except:
                        link_tuples[line] = [(link.replace(" ", "%20"), path)]

        for line, link in ipynb_links:
            link = link.replace("%20", " ")
            if (
                get_file_name(link).replace(".ipynb", "") in notebook_names
            ):  # checking to see if its a rtd notebook
                found_link = False
                for path in docs_paths:
                    if get_file_name(path).replace(".html", ".ipynb") == get_file_name(
                        link
                    ):
                        path = "../" * html_depth + path
                        try:
                            link_tuples[line] = link_tuples[line].append(
                                (link.replace(" ", "%20"), path)
                            )
                        except:
                            link_tuples[line] = [(link.replace(" ", "%20"), path)]
                        found_link = True
                        break
                if not found_link:
                    broken_links.append((rst, link))
            else:  # if not rtd notebook, link gitlab
                link_flag = False
                for notebook in notebooks:
                    if get_file_name(notebook) == get_file_name(link):
                        gitlab_link = besos_examples_link + notebook.replace(" ", "%20")
                        try:
                            link_tuples[line] = link_tuples[line].append(
                                (link.replace(" ", "%20"), gitlab_link)
                            )
                        except:
                            link_tuples[line] = [
                                (link.replace(" ", "%20"), gitlab_link)
                            ]
                        link_flag = True
                        break
                if not link_flag:
                    broken_links.append((rst, link))

        for line, link in py_links:
            link = link.replace("%20", " ")  # rst replaces spaces with %20
            py_flag = False
            for notebook in notebooks:
                if get_file_name(notebook) == get_file_name(link):
                    gitlab_link = besos_examples_link + notebook.replace(" ", "%20")
                    try:
                        link_tuples[line] = link_tuples[line].append(
                            (link, gitlab_link)
                        )
                    except:
                        link_tuples[line] = [(link, gitlab_link)]
                    py_flag = True
                    break
            if not py_flag:
                broken_links.append((rst, link))

        for line, link in html_links:
            link = link.replace("%20", " ")  # rst replaces spaces with %20
            flag = False  # True if link is a working doc link
            for path in docs_paths:
                path = ("../" * html_depth) + path
                if path == link:
                    flag = True
                    break
                if get_file_name(path) == get_file_name(link) and not flag:
                    try:
                        link_tuples[line] = link_tuples[line].append((link, path))
                        flag = True
                    except:
                        link_tuples[line] = [(link, path)]
                        flag = True
            if not flag:  # not a proper local reference
                gitlab_flag = False
                for notebook in notebooks:
                    if get_file_name(notebook).replace(".ipynb", ".html").replace(
                        ".py", ".html"
                    ) == get_file_name(link):
                        gitlab_link = besos_examples_link + notebook.replace(" ", "%20")
                        try:
                            link_tuples[line] = link_tuples[line].append(
                                (link, gitlab_link)
                            )
                        except:
                            link_tuples[line] = [(link, gitlab_link)]
                        gitlab_flag = True
                        break
                if not gitlab_flag:
                    broken_links.append((rst, link))

        readObj.close()
        readObj = open(rst, "r")
        rstText = readObj.read()
        readObj.close()

        for key in link_tuples.keys():
            line = key
            for orig_link, new_link in link_tuples[key]:
                line = line.replace(orig_link, new_link)
                print(get_file_name(rst), ":", "Replacing", orig_link, "with", new_link)
            rstText = rstText.replace(key, line)

        writeObj = open(rst, "w")
        writeObj.write(rstText)
        writeObj.close()

    broken_links = list(dict.fromkeys(broken_links))
    for rst, broken_link in broken_links:
        print(rst, "contained broken link - ", broken_link)


def format_ipynb_title_sizing(notebooks=[]):
    """
    Edits each notebook in notebooks list to contain only one maxsize title
    to ensure readthedocs has correct table of contents
    :param notebooks: A list of paths to notebooks
    """
    if notebooks == []:
        notebooks = glob.glob(
            os.path.join("besos-examples", "**", "*.ipynb"), recursive=True
        )
    for notebook in notebooks:
        readobj = open(notebook, "r")
        jsonText = readobj.read()
        readobj.close()
        jsonObj = json.loads(jsonText)

        indexes = []
        for x in range(len(jsonObj["cells"])):  # loop to gather markdown cells indexes
            if jsonObj["cells"][x]["cell_type"] == "markdown":
                indexes.append(x)

        flag = False  # flag to say already one title with max size
        edited = False  # flag to see if file needs to be overwritten
        for index in indexes:
            for line in jsonObj["cells"][index]["source"]:
                if line.startswith("#") and not line.startswith("##"):
                    if not flag:
                        flag = True
                    else:
                        jsonObj["cells"][index]["source"][
                            jsonObj["cells"][index]["source"].index(line)
                        ] = (
                            "#" + line
                        )  # makes the title one size smaller
                        edited = True
        if edited:
            print("Saving Corrected Changes on", get_file_name(notebook))
            outfile = open(notebook, "w")
            json.dump(jsonObj, outfile, indent=1)
            outfile.write("\n")
            outfile.close()


def get_diff(notebooks):
    """
    Finds notebooks that have changed since py file last ran

    :param notebooks: A list of paths to notebooks
    :return: List of paths to notebooks needing updating
    """
    notebook_diff = []
    unexecuted = Path("reference-nbs")  # path to unexecuted nb's
    for notebook in notebooks:
        unexecuted_cur = Path(
            unexecuted, get_file_name(notebook)
        )  # path to where the unexecuted notebook would be of current notebook
        cmd = ["diff", "-w", unexecuted_cur, notebook]
        return_code = subprocess.run(cmd)  # runs diff ignoring whitespace
        try:  # try block as return_code.check_returncode() will cause raise error if not 0
            return_code.check_returncode()
        except:
            notebook_diff.append(
                notebook
            )  # if an error occured there is a diff in files
    return notebook_diff


def update_rst(notebooks, notebook_outdirs, notebook_names):
    """
    Finds which notebooks have changed (using 'get_diff') then executes and saves the unexecuted & executed notebook files in the correct places

    :param notebooks: A list of paths to notebooks
    :return: None
    """
    notebooks = get_diff(notebooks)

    for notebook in notebooks:  # loop replaces old unexecuted nb with new unexecuted nb
        unexecuted_cur = Path("reference-nbs", get_file_name(notebook))
        shutil.copyfile(notebook, unexecuted_cur)
        remove_whitespace(unexecuted_cur)

    format_ipynb_title_sizing(notebooks)
    print("Updating", len(notebooks), "notebooks")
    for notebook in notebooks:
        print(f"Updating {notebook}")
        subprocess.run(
            [
                "jupyter",
                "nbconvert",
                "--ExecutePreprocessor.timeout=60000",  # made timeout arbitrarily large for long running notebooks
                "--execute",
                notebook,
                "--inplace",
            ]
        )  # executes notebook
        subprocess.run(
            ["jupyter", "nbconvert", notebook, "--to", "rst"]
        )  # converts notebook

        index = notebook_names.index(get_file_name(notebook))
        out_dir = notebook_outdirs[index].replace(".ipynb", ".rst")
        fig_dir = str(notebook).replace(".ipynb", "_files")
        fig_out_dir = notebook_outdirs[index].replace(".ipynb", "_files")
        print("Completed", out_dir)
        shutil.move(str(notebook).replace(".ipynb", ".rst"), out_dir)
        if os.path.exists(fig_dir):  # loop dealing with figures
            if not os.path.exists(fig_out_dir):
                shutil.move(fig_dir, fig_out_dir)
            else:
                for file in os.listdir(fig_dir):
                    file = os.path.join(fig_dir, file)
                    fig_out_file = os.path.join(fig_out_dir, get_file_name(file))
                    shutil.move(file, fig_out_file)
        remove_whitespace(out_dir)
        fix_broken_links()


def main():
    if os.path.exists(os.path.join("besos-examples")):
        raise EnvironmentError(
            "Script will edit and remove 'besos-examples' folder, please remove folder before execution"
        )
    cmd = ["git", "clone", "https://gitlab.com/energyincities/besos-examples.git"]
    subprocess.run(cmd)  # clones besos-examples

    notebook_outdirs = (
        open(Path("rtd-notebooks.txt"), "r").read().split("\n")[:-1]
    )  # reads file to find out which notebooks are expected
    notebook_names = [
        get_file_name(nb) for nb in notebook_outdirs
    ]  # holds file names of each notebook

    notebook_path = os.path.join("besos-examples", "besos", "examples", "**", "*.ipynb")
    all_notebooks = glob.glob(notebook_path, recursive=True)

    notebooks = []  # to contain paths to notebooks contained in rtd-notebooks.txt
    for notebook in all_notebooks:  # loop finds notebooks contained in text file
        if get_file_name(notebook) in notebook_names:
            notebooks.append(notebook)

    if len(sys.argv) > 1:
        if (
            sys.argv[1] == "--update"
        ):  # with --update arg used changes to file system can happen.
            reference_nbs = glob.glob(os.path.join("reference-nbs", "*.ipynb"))
            example_rsts = glob.glob(
                os.path.join("example-notebooks", "**", "*.rst"), recursive=True
            )
            for nb in reference_nbs:  # loop removes unnecessary nb files
                ipynb = get_file_name(nb)
                if not (
                    ipynb in notebook_names
                ):  # if the nb file does not exist in rtd-notebooks.txt, remove it
                    print("Removing", nb)
                    os.remove(nb)
            for nb in example_rsts:  # loop removes unnecessary nb files
                ipynb = nb.replace(".rst", ".ipynb")
                if not (
                    ipynb in notebook_outdirs
                ):  # if the nb file does not exist in rtd-notebooks.txt, remove it
                    print("Removing", nb)
                    os.remove(nb)

            update_rst(notebooks, notebook_outdirs, notebook_names)
        elif sys.argv[1] == "--fixlinks":
            fix_broken_links()
    else:
        diffs = get_diff(notebooks)
        if len(diffs) != 0:
            error_msg = "Notebook have been changed and need to be updated. Run 'python collect_rst --update' to update rst files from notebooks.\n"
            for diff in diffs:
                error_msg += diff + "\n"
            shutil.rmtree("besos-examples")
            raise ValueError(error_msg)
        else:
            print(
                "Ran succesfully! No changes necessary as 'references-nbs' files are up to date with notebooks"
            )

    shutil.rmtree("besos-examples")


if __name__ == "__main__":
    main()
